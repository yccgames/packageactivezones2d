﻿#pragma warning disable
using System.Collections;
using Lean.Touch;
using NSSingleton;
using UnityEngine;

namespace NSActiveZones2D
{
    public class SeleccionarYTransladarObjeto2D : AbstractSingleton<SeleccionarYTransladarObjeto2D>
    {
        [Tooltip("Layer en donde solo estan objetos que usen la clase AbstractObject2D")] 
        [SerializeField] private LayerMask layerMaskSeleccionObjeto2D;

        [Tooltip("Camara desde donde se seleccionan los objetos")] 
        [SerializeField] private Camera refCamera;

        [Tooltip("Radio alrededor del touch que se tiene encuenta para seleccionar los objetos")] 
        [SerializeField] private float radioSeleccionAlrededorDelPuntoDelTouch = 0.2f;

        [Tooltip("Cuanto se tiene que mover el mouse para que se detecte como movimiento")] 
        [SerializeField] private float umbralMovimientoMouseParaMoverObjeto = 0.05f;

        /// <summary>
        /// Objeto actualmente seleccioando
        /// </summary>
        private AbstractObjeto2D refAbstractObjeto2DSelected;

        private float touchScreenDelta;

        [Header("Zonas activas")] 
        [SerializeField] private float velocidadAnimacionZonaActiva;
        
        [SerializeField] private float multiplicadorBrilloAnimacionZonaActivaObjetivo = 0.5f;
        
        [SerializeField, Tooltip("Velocidad con la que se mueve un objeto hacia el mouse que se esta arrastrando ")] private float velocidadMovimientoHaciaMouse = 0.5f;

        /// <summary>
        /// Factor de animacion de las zonas activas
        /// </summary>
        private float multiplicadorAnimacionZonaActiva;

        #region Propierties

        public LayerMask LayerMaskSeleccionObjeto2D => layerMaskSeleccionObjeto2D;

        public float MultiplicadorAnimacionZonaActiva
        {
            get => multiplicadorAnimacionZonaActiva;
        }

        public float MultiplicadorBrilloAnimacionZonaActivaObjetivo => multiplicadorBrilloAnimacionZonaActivaObjetivo;
        
        public float VelocidadMovimientoHaciaMouse => velocidadMovimientoHaciaMouse;
        #endregion

        private void Awake()
        {
            if (!refCamera)
                refCamera = Camera.main;

            StartCoroutine(CouAnimacionZonaActiva());
        }

        private void OnEnable()
        {
            LeanTouch.OnFingerDown += OnFingerDown;
            LeanTouch.OnFingerSet += OnFingerSet;
            LeanTouch.OnFingerTap += OnFingerTab;
            LeanTouch.OnFingerUp += OnFingerUp;
        }

        private void OnDisable()
        {
            LeanTouch.OnFingerDown -= OnFingerDown;
            LeanTouch.OnFingerSet -= OnFingerSet;
            LeanTouch.OnFingerTap -= OnFingerTab;
            LeanTouch.OnFingerUp -= OnFingerUp;
        }

        private void OnFingerDown(LeanFinger argLeanFinger)
        {
            var tmpGuiElements = LeanTouch.RaycastGui(argLeanFinger.ScreenPosition);

            if (tmpGuiElements.Count > 0) //si hay interfaz por encima
                return;

            var tmpPositionOnWorld = refCamera.ScreenToWorldPoint(argLeanFinger.ScreenPosition);
            var tmpCollision2D = Physics2D.OverlapCircle(tmpPositionOnWorld, radioSeleccionAlrededorDelPuntoDelTouch, layerMaskSeleccionObjeto2D);

            if (tmpCollision2D)
            {
                refAbstractObjeto2DSelected = tmpCollision2D.GetComponent<AbstractObjeto2D>();

                if (refAbstractObjeto2DSelected && refAbstractObjeto2DSelected.AccionActual == AccionActual.WaitingForAction && refAbstractObjeto2DSelected.ObjetoSePuedeMover && refAbstractObjeto2DSelected.Interactuable)
                {
                    refAbstractObjeto2DSelected.PosicionObjetivo = tmpPositionOnWorld;
                    refAbstractObjeto2DSelected.ObjetoSeleccionado();
                }
                else
                    refAbstractObjeto2DSelected = null;
            }
        }

        private void OnFingerSet(LeanFinger argLeanFinger)
        {
            touchScreenDelta += argLeanFinger.ScreenDelta.magnitude;

            if (touchScreenDelta > umbralMovimientoMouseParaMoverObjeto)
                if (refAbstractObjeto2DSelected && refAbstractObjeto2DSelected.Interactuable)
                {
                    refAbstractObjeto2DSelected.PosicionObjetivo = refCamera.ScreenToWorldPoint(argLeanFinger.ScreenPosition);
                    refAbstractObjeto2DSelected.ObjetoEstaSeleccionadoYSeEstaTransladando();
                }
        }

        private void OnFingerTab(LeanFinger argLeanFinger)
        {
            var tmpGuiElements = LeanTouch.RaycastGui(argLeanFinger.ScreenPosition);

            if (tmpGuiElements.Count > 0) //si hay interfaz por encima
                return;

            var tmpPositionOnWorld = refCamera.ScreenToWorldPoint(argLeanFinger.ScreenPosition);
            var tmpCollision2D = Physics2D.OverlapCircle(tmpPositionOnWorld, radioSeleccionAlrededorDelPuntoDelTouch, layerMaskSeleccionObjeto2D);

            if (tmpCollision2D)
            {
                refAbstractObjeto2DSelected = tmpCollision2D.GetComponent<AbstractObjeto2D>();

                if (refAbstractObjeto2DSelected && refAbstractObjeto2DSelected.Interactuable)
                {
                    if (refAbstractObjeto2DSelected.AccionActual == AccionActual.WaitingForAction)
                        refAbstractObjeto2DSelected.TabSobreEsteObjeto();

                    return;
                }

                var tmpActiveZone = tmpCollision2D.GetComponent<ZonaActiva>();

                if (tmpActiveZone)
                    tmpActiveZone.OnTab();
            }
        }

        public void OnFingerUp(LeanFinger argLeanFinger)
        {
            touchScreenDelta = 0f;

            if (refAbstractObjeto2DSelected)
                refAbstractObjeto2DSelected.ObjetoDeseleccionado();

            refAbstractObjeto2DSelected = null;
        }
        #region courutines

        private IEnumerator CouAnimacionZonaActiva()
        {
            var tmpAngle = 0f;

            while (true)
            {
                tmpAngle += (180f / velocidadAnimacionZonaActiva) * Time.deltaTime;
                tmpAngle %= 180f;
                multiplicadorAnimacionZonaActiva = Mathf.Sin(tmpAngle * Mathf.Deg2Rad);
                yield return null;
            }
        }
        #endregion
    }
}