﻿#pragma warning disable
using System.Collections;
using System.Collections.Generic;
using NSSingleton;
using UnityEngine;

namespace NSManagerAumentos
{
    public class ManagerAumentos : AbstractSingleton<ManagerAumentos>
    {
        #region members

        [SerializeField] private GameObject aumentoPrincipal;

        private Aumento refAumentoPrincipal;

        [SerializeField] private GameObject[] arrayGameObjectsAumentos;

        [SerializeField] private GameObjectParaOcultar[] arrayGameObjectParaEsconder;

        private Dictionary<string, Aumento> dictionaryAumentos;

        private Stack<Aumento> stackAumentos = new Stack<Aumento>();

        #endregion

        #region properties

        public bool IsInMainAumento => refAumentoPrincipal == stackAumentos.Peek();

        public bool CanShowAumento { get; set; } = true;

        #endregion

        #region MonoBehaviour

        private void Awake()
        {
            refAumentoPrincipal = new Aumento(aumentoPrincipal);
            stackAumentos.Push(refAumentoPrincipal);
            InicializarDictionaryAumentos();
        }

        private IEnumerator Start()
        {
            yield return null;
            refAumentoPrincipal.GuardarEstadoActualPropiedadEnableDelPolygonCollider();

            foreach (var tmpAumento in dictionaryAumentos)
                tmpAumento.Value.GuardarEstadoActualPropiedadEnableDelPolygonCollider();
        }
        #endregion

        #region private methods

        private void InicializarDictionaryAumentos()
        {
            if (dictionaryAumentos == null)
            {
                dictionaryAumentos = new Dictionary<string, Aumento>();

                foreach (var tmpItem in arrayGameObjectsAumentos)
                    dictionaryAumentos.Add(tmpItem.name, new Aumento(tmpItem));
            }
        }
        #endregion

        #region public methods
        
        public void MostrarAumento(string argNombreAumentoQueSeQuiereMostrar, bool argEsconderElAumentoQueQuedaDetras = true)
        {
            if (dictionaryAumentos.ContainsKey(argNombreAumentoQueSeQuiereMostrar))
            {
                if (CanShowAumento)
                {
                    var tmpAumentoPrevio = stackAumentos.Peek();
                    tmpAumentoPrevio.SetInteracionDeTodosLosObjetos2DEnElAumento(false);

                    if (argEsconderElAumentoQueQuedaDetras)
                        tmpAumentoPrevio.OcultarAumento();

                    var tmpAumento = dictionaryAumentos[argNombreAumentoQueSeQuiereMostrar];
                    tmpAumento.SetInteracionDeTodosLosObjetos2DEnElAumento();
                    tmpAumento.MostrarAumento();
                    stackAumentos.Push(tmpAumento);

                    foreach (var tmpGameObjectForHide in arrayGameObjectParaEsconder)
                        tmpGameObjectForHide.Ocultar(tmpAumento.gameObjectAumento);
                }
            }
            else
                Debug.LogError($"El aumento : {argNombreAumentoQueSeQuiereMostrar} agreguelo al array de aumentos.", this);
        }
        
        public bool EsconderUltimoAumentoMostrado()
        {
            var tmpAumentoActual = stackAumentos.Peek();

            if (tmpAumentoActual != refAumentoPrincipal)
            {
                tmpAumentoActual.SetInteracionDeTodosLosObjetos2DEnElAumento(false);
                StartCoroutine(CouHiddeLastAumento(tmpAumentoActual));
                stackAumentos.Pop();

                var tmpAumentoVisible = stackAumentos.Peek();
                tmpAumentoVisible.MostrarAumento();
                tmpAumentoVisible.SetInteracionDeTodosLosObjetos2DEnElAumento();

                foreach (var tmpGameObjectForHide in arrayGameObjectParaEsconder)
                    tmpGameObjectForHide.Mostrar();

                return true;
            }

            return false;
        }
        #endregion

        #region Courutines

        private IEnumerator CouHiddeLastAumento(Aumento argAumentoParaOcultar)
        {
            yield return null;
            argAumentoParaOcultar.OcultarAumento();
        }
        #endregion
    }
}